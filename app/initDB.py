import sqlite3
from app import app

conn = sqlite3.connect(app.database_url)
cursor = conn.cursor()
cursor.execute("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='users'")
if cursor.fetchone()[0] == 0:
	cursor.execute("""CREATE TABLE "users" (
		"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
		"username"	TEXT NOT NULL,
		"password"	TEXT NOT NULL,
		"last_survey"		INTEGER DEFAULT 0
	);
	""")
	cursor.execute("""CREATE TABLE "questions" (
		"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
		"quest"	TEXT NOT NULL,
		"type"	TEXT NOT NULL
	);
	""")
	cursor.execute("""CREATE TABLE "answers" (
		"question_id"	INTEGER NOT NULL,
		"name"	TEXT NOT NULL,
		"value"	TEXT NOT NULL
	)
	""")
	cursor.execute("""CREATE TABLE "results" (
		"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
		"user_id"	INTEGER,
		"last_survey"	INTEGER,
		"question"	INTEGER
	);
	""")
	cursor.execute("""CREATE TABLE "ans" (
		"result_id"	INTEGER,
		"value"	TEXT
	);
	""")
	cursor.execute("INSERT INTO questions (id, quest, type) VALUES (?,?,?)", [1,'Сколько тебе лет?', 'default'])
	cursor.execute("INSERT INTO questions (id, quest, type) VALUES (?,?,?)", [2,'Твоя любимая погода?', 'radio'])
	cursor.execute("INSERT INTO answers (question_id, name, value) VALUES (?,?,?)", [2,'v1', 'Солнечная'])
	cursor.execute("INSERT INTO answers (question_id, name, value) VALUES (?,?,?)", [2,'v2', 'Ветреная'])
	cursor.execute("INSERT INTO answers (question_id, name, value) VALUES (?,?,?)", [2,'v3', 'Пасмурная'])
	cursor.execute("INSERT INTO questions (id, quest, type) VALUES (?,?,?)", [3,'Твоя любимое занятие в свободное время?', 'checkbox'])
	cursor.execute("INSERT INTO answers (question_id, name, value) VALUES (?,?,?)", [3,'v1', 'Спать'])
	cursor.execute("INSERT INTO answers (question_id, name, value) VALUES (?,?,?)", [3,'v2', 'Гулять'])
	cursor.execute("INSERT INTO answers (question_id, name, value) VALUES (?,?,?)", [3,'v3', 'Путешествовать'])
	cursor.execute("INSERT INTO answers (question_id, name, value) VALUES (?,?,?)", [3,'v4', 'Кушать'])
	cursor.execute("INSERT INTO answers (question_id, name, value) VALUES (?,?,?)", [3,'v5', 'Рисовать'])
	cursor.execute("INSERT INTO answers (question_id, name, value) VALUES (?,?,?)", [3,'v6', 'Танцевать'])
	conn.commit()